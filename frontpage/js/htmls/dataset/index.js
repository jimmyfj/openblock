/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

axios({
    url: 'js/htmls/dataset/htmls.html',
    responseType: 'text',
    async: false
}).then(({ data }) => {
    let componentName = 'ob-dataset-sider';
    let template = data;
    Vue.component(componentName, {
        data: function () {
            return {
                importer: OpenBlock.DataImporter
            }
        },
        template: template
    });
    OpenBlock.onInited(() => {
        UB_IDE.addSiderComponent({ name: componentName, icon: 'md-filing', tooltip: OpenBlock.i('数据'), priority: 1 });
    });
}
);