// This file was automatically generated.  Do not modify.

/*jshint -W117 */
Blockly.Msg["WeiXin"] = "微信小程序";
Blockly.Msg["NativeRun"] = "本地执行";

Blockly.Msg['TEXT_KEY_VALUE'] = "按键值 %1 %2";
Blockly.Msg["Character Keys"] = "字符键";
Blockly.Msg["Special Values"] = "特殊值";
Blockly.Msg["Modifier Keys"] = "修饰键";
Blockly.Msg["Whitespace Keys"] = "空白键";
Blockly.Msg["Navigation Keys"] = "导航键";
Blockly.Msg["Editing Keys"] = "编辑键";
Blockly.Msg["UI Keys"] = "UI键";
Blockly.Msg["Device Keys"] = "设备键";
Blockly.Msg["IME and Composition Keys"] = "输入法与组合键";
Blockly.Msg["Function Keys"] = "功能键";
Blockly.Msg["Phone Keys"] = "移动设备键";
Blockly.Msg["Multimedia Keys"] = "多媒体键";
Blockly.Msg["Audio Control Keys"] = "音频控制键";
Blockly.Msg["TV Control Keys"] = "电视控制键";
Blockly.Msg["Media Controller Keys"] = "媒体控制键";
Blockly.Msg["Speech Recognition Keys"] = "语音识别键";
Blockly.Msg["Document Keys"] = "文档键";
Blockly.Msg["Application Selector Keys"] = "应用选择键";
Blockly.Msg["Browser Control Keys"] = "浏览器控制键";
Blockly.Msg["Numeric Keypad Keys"] = "数字键盘键";


/*jshint sub:true*/

Blockly.Msg["StartPoint"] = "起点";
Blockly.Msg["CenterPoint"] = "中心";
Blockly.Msg["unityengine_Vertical"] = "垂直";
Blockly.Msg["unityengine_Horizontal"] = "水平";
Blockly.Msg["TEXT_CHARAT_TAIL"] = "";
Blockly.Msg["TEXT_GET_SUBSTRING_TAIL"] = "";
Blockly.Msg["LISTS_GET_INDEX_TAIL"] = "";
Blockly.Msg["LISTS_GET_SUBLIST_TAIL"] = "";
Blockly.Msg["ORDINAL_NUMBER_SUFFIX"] = "";
Blockly.Msg["PROCEDURES_DEFNORETURN_DO"] = "";


Blockly.Msg['DuplicatedEvent'] = "重复监听事件";
Blockly.Msg['SearchBlock'] = "查找块";


Blockly.Msg['name'] = "名称";
Blockly.Msg['title'] = "标题";

//
//黑色
//预留颜色、547eac、629c1e、9c8c1e、ad4f4f
OpenBlock.setAllBlockStyles({
    "native_call": {
        "colourPrimary": "#1e9c45"
    },
    "control_blocks": {
        "colourPrimary": "#a5745b"
    },
    "operation_blocks": {
        "colourPrimary": "#47868a"
    },
    "message_blocks": {
        "colourPrimary": "#5b6ba5"
    },
    "event_blocks": {
        "colourPrimary": "#a55b5b"
    },
    "string_blocks": {
        "colourPrimary": "#44904a"
    },
    "list_blocks": {
        "colourPrimary": "#5263ab"
    },
    "array_blocks": {
        "colourPrimary": "#0fBD8C"
    },
    "gameobject_blocks": {
        "colourPrimary": "#1e9c45"
    },
    "component_blocks": {
        "colourPrimary": "#9e8144"
    },
    "canvas_blocks": {
        "colourPrimary": "#a56c5b"
    },
    "physical_blocks": {
        "colourPrimary": "#8a6b22"
    },
    "time_blocks": {
        "colourPrimary": "#257976"
    },
    "scene_blocks": {
        "colourPrimary": "#944d67"
    },
    "mobile_blocks": {
        "colourPrimary": "#ab6e8b"
    },
    "output_blocks": {
        "colourPrimary": "#3863a3"
    },
    "input_blocks": {
        "colourPrimary": "#3f8c80"
    },
    "screen_blocks": {
        "colourPrimary": "#bc7e4d"
    },
    "application_blocks": {
        "colourPrimary": "#ab6e6e"
    },
    "audio_blocks": {
        "colourPrimary": "#575E75"
    },
    "date_blocks": {
        "colourPrimary": "#825aa6"
    },
    "LoadAssets_blocks": {
        "colourPrimary": "#408a49"
    },
    "textmeshpro_blocks": {
        "colourPrimary": "#a16c38"
    },
    "network_blocks": {
        "colourPrimary": "#a05aa6"
    },
    "thirdparty_blocks": {
        "colourPrimary": "#8b644b"
    },
    "camera_blocks": {
        "colourPrimary": "#1cb479"
    },
    "database_blocks": {
        "colourPrimary": "#625eaa"
    },
    "rank_blocks": {
        "colourPrimary": "#956732"
    },
    "struct_blocks": {
        "colourPrimary": "#3c8170"
    },
    "colour_blocks": {
        "colourPrimary": "20"
    },
    "logic_blocks": {
        "colourPrimary": "#a5745b"
    },
    "loop_blocks": {
        "colourPrimary": "#a5745b"
    },
    "math_blocks": {
        "colourPrimary": "#47868a"
    },
    "navmesh_blocks": {
        "colourPrimary": "#327e74"
    },
    "procedure_blocks": {
        "colourPrimary": "#375a7d"
    },
    "text_blocks": {
        "colourPrimary": "#44904a"
    },
    "variable_blocks": {
        "colourPrimary": "#377d5b"
    },
    "variable_dynamic_blocks": {
        "colourPrimary": "#7f4c5f"
    },
    "variable_diy_blocks": {
        "colourPrimary": "#71377d"
    },
    "hat_blocks": {
        "colourPrimary": "330",
        "hat": "cap"
    },

    "shadow_blocks": {
        "colourPrimary": "#404040"
    },
})
//分组颜色-黑

OpenBlock.setCategoryStyle("control_category", {
    "colour": "#a5745b"
})
OpenBlock.setCategoryStyle("native_call_category", {
    "colour": "#1e9c45"
})
OpenBlock.setCategoryStyle("navmesh_category", {
    "colour": "#327e74"
})
OpenBlock.setCategoryStyle("operation_category", {
    "colour": "#47868a"
})
OpenBlock.setCategoryStyle("message_category", {
    "colour": "#5b6ba5"
})
OpenBlock.setCategoryStyle("string_category", {
    "colour": "#44904a"
})
OpenBlock.setCategoryStyle("list_category", {
    "colour": "#5263ab"
})
OpenBlock.setCategoryStyle("event_category", {//1d7835
    "colour": "#a55b5b"
})
OpenBlock.setCategoryStyle("array_category", {
    "colour": "#0fBD8C"
})
OpenBlock.setCategoryStyle("gameobject_category", {
    "colour": "#388757"
})
OpenBlock.setCategoryStyle("component_category", {
    "colour": "#9e8144"
})
OpenBlock.setCategoryStyle("canvas_category", {
    "colour": "#a56c5b"
})
OpenBlock.setCategoryStyle("physical_category", {
    "colour": "#8a6b22"
})
OpenBlock.setCategoryStyle("time_category", {
    "colour": "#257976"
})
OpenBlock.setCategoryStyle("scene_category", {
    "colour": "#944d67"
})
OpenBlock.setCategoryStyle("mobile_category", {
    "colour": "#ab6e8b"
})
OpenBlock.setCategoryStyle("output_category", {
    "colour": "#3863a3"
})
OpenBlock.setCategoryStyle("input_category", {
    "colour": "#3f8c80"
})
OpenBlock.setCategoryStyle("screen_category", {
    "colour": "#bc7e4d"
})
OpenBlock.setCategoryStyle("application_category", {
    "colour": "#ab6e6e"
})
OpenBlock.setCategoryStyle("audio_category", {
    "colour": "#575E75"
})
OpenBlock.setCategoryStyle("date_category", {
    "colour": "#825aa6"
})
OpenBlock.setCategoryStyle("LoadAssets_category", {
    "colour": "#408a49"
})
OpenBlock.setCategoryStyle("textmeshpro_category", {
    "colour": "#a16c38"
})
OpenBlock.setCategoryStyle("network_category", {
    "colour": "#a05aa6"
})
OpenBlock.setCategoryStyle("thirdparty_category", {
    "colour": "#8b644b"
})
OpenBlock.setCategoryStyle("camera_category", {
    "colour": "#1cb479"
})
OpenBlock.setCategoryStyle("database_category", {
    "colour": "#625eaa"
})
OpenBlock.setCategoryStyle("rank_category", {
    "colour": "#3c8170"
})
OpenBlock.setCategoryStyle("diytype_category", {
    "colour": "#518377"
})
OpenBlock.setCategoryStyle("colour_category", {
    "colour": "20"
})
OpenBlock.setCategoryStyle("logic_category", {
    "colour": "210"
})
OpenBlock.setCategoryStyle("loop_category", {
    "colour": "120"
})
OpenBlock.setCategoryStyle("math_category", {
    "colour": "#47868a"
})
OpenBlock.setCategoryStyle("procedure_category", {
    "colour": "375a7d"
})
OpenBlock.setCategoryStyle("text_category", {
    "colour": "160"
})
OpenBlock.setCategoryStyle("variable_category", {
    "colour": "377d5b"
})
OpenBlock.setCategoryStyle("variable_dynamic_category", {
    "colour": "7f4c5f"
})
OpenBlock.setCategoryStyle("variable_diy_category", {
    "colour": "#71377d"
})
OpenBlock.setCategoryStyle("variable_state_category", {
    "colour": "#7d3737"
})
OpenBlock.setCategoryStyle("parameter_category", {
    "colour": "#3c377d"
})