'use strict';

Blockly.Msg["MESSAGE"] = "消息";
Blockly.Msg["FSM_CTRL"] = "状态机控制";
Blockly.Msg["EVENT"] = "事件";
Blockly.Msg["DEBUG"] = "调试";
Blockly.Msg["TEXT"] = "文字";
Blockly.Msg["BOOLEAN"] = "布尔";
Blockly.Msg["LOOPS"] = "循环";
Blockly.Msg["MATH"] = "数学";
Blockly.Msg["LISTS"] = "列表";
Blockly.Msg["STRUCTS"] = "数据结构";
Blockly.Msg["METHOD"] = "函数";
Blockly.Msg["VAR_FSM"] = "状态机变量";
Blockly.Msg["VAR_STATE"] = "状态变量";
Blockly.Msg["VAR_LOCAL"] = "局部变量";
