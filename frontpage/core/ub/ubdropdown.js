/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */


//// +++++++++++++
OpenBlock.UBFieldDropdown = function (opt_config) {
    // Call parent's constructor.
    // OpenBlock.UBFieldDropdown.superClass_.constructor.call(this, OpenBlock.UBFieldDropdown.menuGenerator);
}
Blockly.utils.object.inherits(OpenBlock.UBFieldDropdown, Blockly.FieldDropdown);
OpenBlock.UBFieldDropdown.menuGenerator = function () {
    // subClass have to override this
    // return [['', '']];
};
OpenBlock.UBFieldDropdown.prototype.doClassValidation_ = function (a) {
    return a;
};

/**
 * Update the value of this dropdown field.
 * @param {*} newValue The value to be saved. The default validator guarantees
 * that this is one of the valid dropdown options.
 * @protected
 */
OpenBlock.UBFieldDropdown.prototype.doValueUpdate_ = function (newValue) {
    OpenBlock.UBFieldDropdown.superClass_.doValueUpdate_.call(this, newValue);
    var options = this.getOptions(true);
    for (var i = 0, option; (option = options[i]); i++) {
        if (option[1] === this.value_) {
            this.selectedOption_ = option;
            let blk = this.getSourceBlock();
            if (blk) {
                blk.setWarningText();
            }
            return;
        }
    }
    this.selectedOption_ = [newValue, newValue];
    let blk = this.getSourceBlock();
    if (blk) {
        blk.setWarningText('找不到变量 ' + newValue);
    }
};
OpenBlock.UBFieldDropdown.prototype.toXml = function (fieldElement) {
    fieldElement.textContent = this.getValue();
    return fieldElement;
};
OpenBlock.UBFieldDropdown.prototype.fromXml = function (fieldElement) {
    this.setValue(fieldElement.textContent);
};
OpenBlock.UBFieldDropdown.prototype.setValue = function (newValue) {
    Blockly.Field.prototype.setValue.call(this, newValue);
}
OpenBlock.UBFieldDropdown.prototype.getOptions = function (opt_useCache) {
    return OpenBlock.UBFieldDropdown.menuGenerator.call(this);
}
OpenBlock.UBFieldDropdown.fromJson = function (options) {
    return new OpenBlock.UBFieldDropdown(options);
};

    //// -------------